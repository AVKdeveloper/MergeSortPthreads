#include <assert.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "merge_sort.h"
#include "utils_integer.h"

int main(int argc, char **argv) {
    // Input parameters
    if (argc < 4 || argc > 6) {
        printf("Wrong number of arguments!\n");
        exit(1);
    }
    int array_length = atoi(argv[1]);
    if (array_length < 1) {
        printf("Incorrect value of array length!\n");
        exit(1);
    }
    int max_chunk_length = atoi(argv[2]);
    if (max_chunk_length < 1) {
        printf("Incorrect value of max chunk length!\n");
        exit(1);
    }
    int number_threads = atoi(argv[3]);
    if (number_threads < 1) {
        printf("Incorrect value of threads number!\n");
        exit(1);
    }
    char *file_name = "stats.txt";
    if (argc >= 5) {
        file_name = argv[4];
    }
    bool need_check = false;
    if (argc >= 6) {
        need_check = true;
    }
    // Logic part
    // Initializing array and data file
    size_t size = sizeof(int);
    int *array = (int *) malloc(size * array_length);
    int * supporting_array;
    InitializeByRandomNumbers(array, array_length);
    if (need_check) {
        supporting_array = (int *) malloc(size * array_length);
        memcpy(supporting_array, array, size * array_length);
        qsort((void *)supporting_array, array_length, size, CompareInt);
    }
    FILE *data_file = fopen("data.txt", "w");
    for (int i = 0; i < array_length; ++i) {
  	     fprintf(data_file, "%d ", array[i]);
    }
    // Measure time
    double result_time = omp_get_wtime();
    MergeSort((void *)array, array_length, size, max_chunk_length,
              CompareInt, number_threads); 
    result_time = omp_get_wtime() - result_time;
    assert(isSorted((void *)array, array_length, size, CompareInt));
    if (need_check) {
        assert(memcmp(supporting_array, array, size * array_length) == 0);
    }
    // Output
    fprintf(data_file, "\n");
    for (int i = 0; i < array_length; ++i) {
  	     fprintf(data_file, "%d ", array[i]);
    }
    fclose(data_file);
    FILE *results_file = fopen(file_name, "a");
    fprintf(results_file, "%fs %d %d %d\n", result_time, array_length,
        max_chunk_length, number_threads);
    fclose(results_file);
    if (need_check) {
        free(supporting_array);
    }
    free(array);
    return 0;
}
