#ifndef MERGE_SORT_H
#define MERGE_SORT_H

#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>

typedef struct merge_params_t {
    void *array1;
    size_t length1;
    void *array2;
    size_t length2;
    void *resulting_array;
    size_t size;
    size_t max_chunk_length;
    int (*compare)(const void *, const void *);
    sem_t *sem_ptr;
} merge_params_t;

typedef struct merge_sort_params_t {
    void *array;
    size_t length;
    size_t size;
    size_t max_chunk_length;
    int (*compare)(const void *, const void *);
    void *tmp_array;
    sem_t *sem_ptr;
} merge_sort_params_t;

bool isSorted(const void *array, size_t length, size_t size,
              int (*compare)(const void *, const void *)) {
    for (int i = 0; i < length - 1; ++i) {
        if (compare((void *)((char *)(array) + i * size),
                    (void *)((char *)(array) + (i + 1) * size)) > 0) {
            return false;
        }
    }
    return true;
}

size_t BinarySearch(const void *array, size_t length, size_t size, void *key,
                    int (*compare)(const void *, const void *)) {
    // The function returns the index of first element which is >= key
    int left = -1;
    int right = length;
    while (left < right - 1) {
        int middle = (left + right) / 2;
        if (compare((void *)((char *)(array) + middle * size), key) >= 0) {
            right = middle;
        } else {
            left = middle;
        }
    }
    return right;
}

void UsualMerge(void *array1, size_t length1, void *array2, size_t length2,
                void *resulting_array, size_t size,
                int (*compare)(const void *, const void *)) {
    // Merge two sorted arrays to resulting array, which has the right size
    size_t idx1 = 0;
    size_t idx2 = 0;
    while (idx1 < length1 && idx2 < length2) {
        if (compare((void *)((char *)(array1) + idx1 * size),
                    (void *)((char *)(array2) + idx2 * size)) <= 0) {
            memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
                   (void *)((char *)(array1) + idx1 * size), size);
            idx1++;
        } else {
            memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
                   (void *)((char *)(array2) + idx2 * size), size);
            idx2++;
        }
    }
    memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
           (void *)((char *)(array1) + idx1 * size), (length1 - idx1) * size);
    memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
           (void *)((char *)(array2) + idx2 * size), (length2 - idx2) * size);
}

void *ParallelMerge(void *parameters) {
    // Merge two sorted arrays to resulting array, which has the right size
    merge_params_t *params = (merge_params_t *)parameters;
    if (params->length1 + params->length2 <= params->max_chunk_length) {
        UsualMerge(
            params->array1,
            params->length1,
            params->array2,
            params->length2,
            params->resulting_array,
            params->size,
            params->compare
        );
        return NULL;
    }
    if (params->length1 == 0) {
        memcpy(
            params->resulting_array,
            params->array2,
            params->length2 * params->size
        );
        return NULL;
    }
    size_t mid1 = params->length1 / 2;
    size_t mid2 = BinarySearch(
        params->array2,
        params->length2,
        params->size,
        (void *)((char *)(params->array1) + mid1 * params->size),
        params->compare
    );
    size_t mid3 = mid1 + mid2;
    memcpy(
        (void *)((char *)(params->resulting_array) + mid3 * params->size),
        (void *)((char *)(params->array1) + mid1 * params->size),
        params->size
    );
    merge_params_t first_params;
    first_params.array1 = params->array1;
    first_params.length1 = mid1;
    first_params.array2 = params->array2;
    first_params.length2 = mid2;
    first_params.resulting_array = params->resulting_array;
    first_params.size = params->size;
    first_params.max_chunk_length = params->max_chunk_length;
    first_params.compare = params->compare;
    first_params.sem_ptr = params->sem_ptr;
    merge_params_t second_params;
    second_params.array1 = (void *)((char *)(params->array1)
                                    + (mid1 + 1) * params->size);
    second_params.length1 = params->length1 - (mid1 + 1);
    second_params.array2 = (void *)((char *)(params->array2)
                                    + mid2 * params->size);
    second_params.length2 = params->length2 - mid2;
    second_params.resulting_array = (void *)((char *)(params->resulting_array)
                                             + (mid3 + 1) * params->size);
    second_params.size = params->size;
    second_params.max_chunk_length = params->max_chunk_length;
    second_params.compare = params->compare;
    second_params.sem_ptr = params->sem_ptr;
    if (sem_trywait(params->sem_ptr) == 0) {
        pthread_t thread;
        pthread_create(&thread, NULL, ParallelMerge, (void *)&second_params);
        ParallelMerge((void *)&first_params);
        pthread_join(thread, NULL);
        sem_post(params->sem_ptr);
    } else {
        ParallelMerge((void *)&first_params);
        ParallelMerge((void *)&second_params);
    }
    return NULL;
}

void *MergeSortWithArray(void *parameters) {
    merge_sort_params_t *params = (merge_sort_params_t *)parameters;
    if (params->length <= params->max_chunk_length) {
        qsort(params->array, params->length, params->size, params->compare);
    } else {
        size_t new_length = params->length / 2;
        merge_sort_params_t first_params;
        first_params.array = params->array;
        first_params.length = new_length;
        first_params.size = params->size;
        first_params.max_chunk_length = params->max_chunk_length;
        first_params.compare = params->compare;
        first_params.tmp_array = params->tmp_array;
        first_params.sem_ptr = params->sem_ptr;
        merge_sort_params_t second_params;
        second_params.array = (void *)((char *)(params->array)
                                       + new_length * params->size);
        second_params.length = params->length - new_length;
        second_params.size = params->size;
        second_params.max_chunk_length = params->max_chunk_length;
        second_params.compare = params->compare;
        second_params.tmp_array = (void *)((char *)(params->tmp_array) +
                                           new_length * params->size);
        second_params.sem_ptr = params->sem_ptr;
        merge_params_t merge_params;
        merge_params.array1 = params->array;
        merge_params.length1 = new_length;
        merge_params.array2 = (void *)((char *)(params->array)
        + new_length * params->size);
        merge_params.length2 = params->length - new_length;
        merge_params.resulting_array = params->tmp_array;
        merge_params.size = params->size;
        merge_params.max_chunk_length = params->max_chunk_length;
        merge_params.compare = params->compare;
        merge_params.sem_ptr = params->sem_ptr;
        if (sem_trywait(params->sem_ptr) == 0) {
            pthread_t thread;
            pthread_create(&thread, NULL, MergeSortWithArray,
                           (void *)&second_params);
            MergeSortWithArray((void *)&first_params);
            pthread_join(thread, NULL);
            sem_post(params->sem_ptr);
        } else {
            MergeSortWithArray((void *)&first_params);
            MergeSortWithArray((void *)&second_params);
        }
        ParallelMerge((void *)&merge_params);
        memcpy(params->array, params->tmp_array, params->length * params->size);
    }
    return NULL;
}

void MergeSort(void *array, size_t length, size_t size, size_t max_chunk_length,
               int (*compare)(const void *, const void *),
               size_t number_threads) {
    void *tmp_array = malloc(size * length);
    sem_t semaphore;
    sem_init(&semaphore, 0, number_threads - 1);
    merge_sort_params_t params;
    params.array = array;
    params.length = length;
    params.size = size;
    params.max_chunk_length = max_chunk_length;
    params.compare = compare;
    params.tmp_array = tmp_array;
    params.sem_ptr = &semaphore;
    MergeSortWithArray((void *)&params);
    sem_destroy(&semaphore);
    free(tmp_array);
}

 #endif  // MERGE_SORT_H_
