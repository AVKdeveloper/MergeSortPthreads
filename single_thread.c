#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "utils_integer.h"

int main(int argc, char **argv) {
    // Input parameters
    if (argc < 2 || argc > 3) {
        printf("Wrong number of arguments!\n");
        exit(1);
    }
    int array_length = atoi(argv[1]);
    if (array_length < 1) {
        printf("Incorrect value of array length!\n");
        exit(1);
    }
    char *file_name = "stats.txt";
    if (argc >= 3) {
        file_name = argv[2];
    }
    // Logic part
    // Initializing array and data file
    int *array = (int *) malloc(sizeof(int) * array_length);
    InitializeByRandomNumbers(array, array_length);
    FILE *data_file = fopen("data.txt", "w");
    for (int i = 0; i < array_length; ++i) {
  	     fprintf(data_file, "%d ", array[i]);
    }
    // Measure time
    double result_time = omp_get_wtime();
  	qsort((void *)array, array_length, sizeof(int), CompareInt);
    result_time = omp_get_wtime() - result_time;
    // Output
    fprintf(data_file, "\n");
    for (int i = 0; i < array_length; ++i) {
  	     fprintf(data_file, "%d ", array[i]);
    }
    fclose(data_file);
    FILE *results_file = fopen(file_name, "a");
    fprintf(results_file, "%fs %d\n", result_time, array_length);
    fclose(results_file);
    free(array);
    return 0;
}
