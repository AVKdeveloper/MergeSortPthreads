#ifndef UTILS_INTEGER_H_
#define UTILS_INTEGER_H_

#include <stdlib.h>
#include <time.h>

int CompareInt(const void *a, const void *b) {
    return *(int *)a - *(int *)b;
}

void InitializeByRandomNumbers(int *array, size_t length) {
  srand(time(NULL));
  for (int i = 0; i < length; ++i) {
	array[i] = rand();
  }
}

void InitializeByShortRandomNumbers(int *array, size_t length) {
  srand(time(NULL));
  for (int i = 0; i < length; ++i) {
	array[i] = rand() % 10;
  }
}

void PrintArray(int *array, size_t length) {
    printf("( ");
    for (int i = 0; i < length; ++i) {
        printf("%d ", array[i]);
    }
    printf(")\n");
}

#endif  // UTILS_INTEGER_H_
